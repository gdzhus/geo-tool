getRoute <- function(sourceAddr, destAddr){
  
  require(rjson)  
  origin <- sourceAddr
  destination <- destAddr
  travelMode <- "car"
  departureTime <- Sys.time() #I want to leave now!
  
  # build the URL
  baseUrl <- "http://maps.googleapis.com/maps/api/directions/json?"
  #parse out the spaces in the url codes
  origin <- gsub(" ", "+", origin)
  destination <- gsub(" ", "+", destination)
  finalUrl <- paste(baseUrl
                    , "origin=", origin
                    , "&destination=", destination
                    , "&sensor=false"
                    , "&mode=", travelMode
                    , "&departure_time=", as.integer(departureTime)
                    , sep = "")
  
  # get the JSON returned by Google and convert it to an R list
  url_string <- URLencode(finalUrl)
  route <- fromJSON(paste(readLines(url_string), collapse = ""))
    
  return(route)
}