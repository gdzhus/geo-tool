library(shiny)

# Define UI for application that plots random distributions 
shinyUI(fluidPage(
  
  # Application title
  titlePanel("Deloitte Crime Location v0.1"),
  
  # Sidebar with a slider input for number of observations
  sidebarLayout(
    sidebarPanel(
      h4("Please enter a location."),
      textInput(inputId="sourceLocation", label="Location", value="Bangor, Northern Ireland"),
      sliderInput(inputId="zoom1", label="Zoom Parameter", min=1, max=21, value=14, step=1)
    ),
    
    # Show a plot of the generated distribution
    mainPanel(  
      p('A map that shows the locations of all open and closed crimes in the month
        of April 2016 in the area that you have selected.'),
      plotOutput("locationMap", width="770px", height="550px"),
      h3("Details:"),    
      tableOutput("routeSummary"),
      plotOutput("Crimeplot")
      #h3("Road List:"),    
      #verbatimTextOutput("roadList" )
      )
  )
))
